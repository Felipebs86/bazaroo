<?php
    require_once( "./comum.php");
    require_once( BASE_DIR . "/classes/Categoria.php");

    $id = $_POST["id"];
    $descricao = $_POST["descricao"];

    $statement = Categoria::alterarCategoria($id, $descricao);

    //$result = $statement->execute();

    if( $statement )
    {
      header("Location: categoria.php?sucesso=Categoria " . urlencode($descricao) . " alterada para $descricao com sucesso.");
    }
?>
