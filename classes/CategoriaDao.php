<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait CategoriaDao
{
  public static function rowMapper($idCategoria, $descricao, $taxa)
  {
    return new Categoria( $idCategoria, $descricao, $taxa);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT idCategoria,descricao,taxa FROM Categoria");
      $statement->execute();
      return $statement->fetchAll( PDO::FETCH_FUNC, "CategoriaDao::rowMapper" );
  }

  public static function adicionarCategoria( Categoria $categoria)
  {
    	$pdo = Banco::obterConexao();
    	$statement = $pdo->query("select max(idCategoria) as maior_id from Categoria");
    	$registro = $statement->fetch();

    	$id = $registro["maior_id"];

      echo $id . " testando numero";

    	if( $id == null ){
        $novo_id = 1;
      }
    	else{
        $novo_id = $id + 1;
      }

      echo $novo_id;

    	$insere = $pdo->prepare("insert into Categoria (idCategoria, descricao, taxa) values (:id, :descricao, :taxa)");
    	$insere->bindParam( ":id", $novo_id, PDO::PARAM_INT);
    	$insere->bindParam( ":descricao", $categoria->getDescricao(), PDO::PARAM_STR);
    	$insere->bindParam( ":taxa", str_replace( ',','.', $categoria->getTaxa() ), PDO::PARAM_STR);
    	return $insere->execute();
  }

  public static function excluirCategoria($id)
  {
    $pdo = Banco::obterConexao();

    $exclui = $pdo->prepare("delete from Categoria where idCategoria = :id");
    $exclui->bindParam( ":id", $id, PDO::PARAM_INT);
    return $exclui->execute();
  }

  public static function alterarCategoria($id, $descricao)
  {
    $pdo = Banco::obterConexao();

    $altera = $pdo->prepare("update Categoria set descricao=:descricao where idCategoria = :id");
    $altera->bindParam( ":id", $id, PDO::PARAM_INT);
    $altera->bindParam( ":descricao", $descricao, PDO::PARAM_STR);
    return $altera->execute();
  }
}
