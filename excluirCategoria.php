<?php
    require_once( "./comum.php");
    require_once( BASE_DIR . "/classes/Categoria.php");

    $id = $_GET["id"];
    $descricao = $_GET["descricao"];

    $result = Categoria::excluirCategoria($id);

    if ($result) {
      header("Location: categoria.php?sucesso=Categoria " . urlencode($descricao) . " excluída com sucesso");
    }

?>
