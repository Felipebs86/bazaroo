<?php

  $descricao = $_POST["descricao"];

  $taxa = $_POST["taxa"];

  $categoria = Categoria->rowMapper(null, $descricao, $taxa);

  $resultado = Categoria::adicionarCategoria($categoria);

	if( $resultado ) {
		header("Location: categoria.php?sucesso=Categoria " . urlencode($descricao) . " criada com sucesso." );
	}
