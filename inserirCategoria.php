<?php
  require_once( "./comum.php");
  require_once( BASE_DIR . "/classes/Categoria.php");

  $descricao = $_POST["descricao"];
  $taxa = $_POST["taxa"];

  $categoria = new Categoria( null, $descricao, $taxa);
	$result = Categoria::adicionarCategoria( $categoria);

  if ($result) {
    header("Location: categoria.php?sucesso=Categoria " . urlencode($descricao) . " criada com sucesso." );
  }

?>
